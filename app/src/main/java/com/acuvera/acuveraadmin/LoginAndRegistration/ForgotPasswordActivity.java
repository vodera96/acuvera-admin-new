package com.acuvera.acuveraadmin.LoginAndRegistration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.acuvera.acuveraadmin.R;

public class ForgotPasswordActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);
    }

    public void backhome(View view) {
        ForgotPasswordActivity.this.finish();
    }
}
