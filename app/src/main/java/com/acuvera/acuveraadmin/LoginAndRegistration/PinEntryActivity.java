package com.acuvera.acuveraadmin.LoginAndRegistration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acuvera.acuveraadmin.Dashboard.DashboardActivity;
import com.acuvera.acuveraadmin.R;
import com.chaos.view.PinView;

public class PinEntryActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    String isregistered;

    SharedPreferences.Editor editor;
    TextView textView;
    PinView pv;
    LinearLayout linearLayout;
    Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_entry);
        pv = (PinView) findViewById(R.id.pinView);

        linearLayout = (LinearLayout) findViewById(R.id.confirmLayout);
        textView = (TextView) findViewById(R.id.mainText);
        button = (Button) findViewById(R.id.submitButton);


        sharedPreferences = getSharedPreferences("PIN", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        isregistered = sharedPreferences.getString("registered", "false");
        if (isregistered.equals("true")) {
            linearLayout.setVisibility(View.GONE);
            textView.setText("Enter Pin");
            button.setText("Submit");
        }
    }

    public void dashboard(View view) {
        editor.putString("registered", "true");
        editor.commit();
        startActivity(new Intent(PinEntryActivity.this, DashboardActivity.class));
        this.finish();
    }
}
