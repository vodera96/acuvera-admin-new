package com.acuvera.acuveraadmin.LoginAndRegistration;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.acuvera.acuveraadmin.Dashboard.DashboardActivity;
import com.acuvera.acuveraadmin.R;
import com.acuvera.acuveraadmin.http.UserFunctions;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    String isregistered;
    private static String KEY_SUCCESS = "status";
    String token;
    EditText phone, password;
    TextInputLayout phonelayer, passwordlayer;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String[] Tags, Values;
    AlertDialog.Builder builder;
    String status;
    String gottoken;
    String httptype = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phone = (EditText) findViewById(R.id.phoneNumber);

        password = (EditText) findViewById(R.id.password);
        phonelayer = (TextInputLayout) findViewById(R.id.phoneLayer);
        passwordlayer = (TextInputLayout) findViewById(R.id.passwordlayer);
        passwordlayer.setPasswordVisibilityToggleEnabled(true);
        builder = new AlertDialog.Builder(this);
        sharedPreferences = getSharedPreferences("PIN", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        Tags = new String[]{};
        Values = new String[]{};
        httptype = "get";
        checkInternetConnection();
        isregistered = sharedPreferences.getString("registered", "false");

    }

    public void createaccount(View view) {
        String url = "http://demo.pulsans.com/acuvera/public/login#";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }

    public void signin(View view) {
        httptype = "post";
        if (phone.getText().toString().isEmpty()) {
            phonelayer.setErrorEnabled(true);
            phonelayer.setError("Please Enter number");
        } else if (password.getText().toString().isEmpty()) {
            passwordlayer.setErrorEnabled(true);
            passwordlayer.setError("Enter Password");
        } else if (phone.getText().toString().length() < 9) {
            phonelayer.setCounterEnabled(true);
            phonelayer.setError("Enter Valid number");
        } else if (password.getText().toString().length() < 4) {
            passwordlayer.setPasswordVisibilityToggleEnabled(true);
            passwordlayer.setError("invalid password");
        } else {
            Tags = new String[]{"token", "phone", "pin"};
            Values = new String[]{gottoken, phone.getText().toString(), password.getText().toString()};
            checkInternetConnection();
        }

    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {

            new MainActivity.GetToken().execute();
            return true;
        } else {
            builder.setTitle("No Internet Connection");
            builder.setMessage("Please check your internet settings!!");
            builder.setPositiveButton("TRY AGAIN", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    checkInternetConnection();
                }
            });
            builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
            return false;
        }
    }

    public void ForgotPassword(View view) {
        Intent forgotPassword = new Intent(MainActivity.this, ForgotPasswordActivity.class);
        startActivity(forgotPassword);

    }

    private class GetToken extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pDialog;
        private MainActivity.GetToken createTask = null;

        @Override
        protected void onPreExecute() {
            createTask = this;
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setTitle("Authentication Ongoing");
            pDialog.setMessage("Please wait..");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled()) {
            } else {
                UserFunctions userFunction = new UserFunctions("auth", httptype);
                JSONObject json = null;
                try {
                    json = userFunction.GenericFunction(Tags, Values);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (json.getString(KEY_SUCCESS) != null) {
                        String res = json.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200) {
                            status = "1";
                            token = json.getString("data");

                        } else if (Integer.parseInt(res) == 400) {
                            status = "0";
                        }
                    }
                } catch (JSONException e1) {
                    Log.v("1", e1.toString());
                    status = "3";
                } catch (ParseException e1) {
                    Log.v("2", e1.toString());
                    status = "3";
                } catch (Exception e) {
                    Log.v("3", e.toString());
                    status = "3";
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
            if (status == "1") {
                if (httptype.equals("get")) {
                    Log.e("token", "got " + token);
                    SharedPreferences tokenSP = getSharedPreferences("token", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed = tokenSP.edit();
                    Toast.makeText(MainActivity.this, "Token retrieved", Toast.LENGTH_SHORT).show();
                    try {
                        JSONObject respo = new JSONObject(token);
                        gottoken = respo.opt("token").toString();
                        Log.e("mytoken", "token number " + gottoken);
                        ed.putString("token", gottoken);
                        ed.commit();
                        ed.apply();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (httptype.equals("post")) {
                    startActivity(new Intent(MainActivity.this, DashboardActivity.class));
                }
            }
            if (status == "0") {
                builder.setTitle("Action Failed");
                builder.setMessage("Please try again later");
                builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
            if (status == "3") {
                builder.setTitle("Action Failed");
                builder.setMessage("Connection Error");
                builder.setPositiveButton("Try Again Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        }

    }
}
