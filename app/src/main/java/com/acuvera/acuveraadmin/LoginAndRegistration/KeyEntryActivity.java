package com.acuvera.acuveraadmin.LoginAndRegistration;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.acuvera.acuveraadmin.R;
import com.acuvera.acuveraadmin.http.UserFunctions;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class KeyEntryActivity extends Activity {
    AlertDialog.Builder builder;
    String[] Tags, Values;
    String KEY_SUCCESS = "status";
    String token;
    String status = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_entry);
        SharedPreferences tokenSP = getSharedPreferences("token", Context.MODE_PRIVATE);
        token = tokenSP.getString("token", "null");
        builder = new AlertDialog.Builder(this);
    }

    public void pin(View view) {
        //startActivity(new Intent(KeyEntryActivity.this, PinEntryActivity.class));
        Tags = new String[]{"token", "phone", "pin"};

        Values = new String[]{token, "254718809966", "8080"};
        checkInternetConnection();
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            new KeyEntryActivity.Login_Task().execute();
            return true;
        } else {
            builder.setTitle("No Internet Connection");
            builder.setMessage("Please check your internet settings!!");
            builder.setPositiveButton("TRY AGAIN", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    checkInternetConnection();
                }
            });
            builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
            return false;
        }
    }


    private class Login_Task extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pDialog;
        private KeyEntryActivity.Login_Task createTask = null;

        @Override
        protected void onPreExecute() {
            createTask = this;
            pDialog = new ProgressDialog(KeyEntryActivity.this);
            pDialog.setTitle("Authentication Ongoing");
            pDialog.setMessage("Please wait..");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled()) {
            } else {
                UserFunctions userFunction = new UserFunctions("auth", "post");
                JSONObject json = null;
                try {
                    json = userFunction.GenericFunction(Tags, Values);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (json.getString(KEY_SUCCESS) != null) {
                        String res = json.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200) {
                            status = "1";
                        } else if (Integer.parseInt(res) == 400) {
                            status = "0";
                        }
                    }
                } catch (JSONException e1) {
                    Log.v("1", e1.toString());
                    status = "3";
                } catch (ParseException e1) {
                    Log.v("2", e1.toString());
                    status = "3";
                } catch (Exception e) {
                    Log.v("3", e.toString());
                    status = "3";
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
            if (status == "1") {
                startActivity(new Intent(KeyEntryActivity.this,
                        PinEntryActivity.class));
                KeyEntryActivity.this.finish();

            }
            if (status == "0") {
                builder.setTitle("Action Failed");
                builder.setMessage("Please try again later");
                builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
            if (status == "3") {
                builder.setTitle("Action Failed");
                builder.setMessage("Connection Error");
                builder.setPositiveButton("Try Again Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        }

    }
}
